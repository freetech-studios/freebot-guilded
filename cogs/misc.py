import guilded
import yaml

from guilded.ext import commands

with open('config.yaml', 'r') as config_file:
    config = yaml.load(config_file, Loader=yaml.BaseLoader)

class Misc(commands.Cog, name="Misc"):
  def __init__(self, client):
      self.client = client

  @commands.command()
  async def ping(self, ctx):
      """Replies with the bot latency."""
      await ctx.send(f"**🏓 Pong!** Latency: {round(self.client.latency * 1000)}ms")

def setup(client):
  client.add_cog(Misc(client))
