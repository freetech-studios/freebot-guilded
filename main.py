# Import libraries
import guilded
import os
import yaml

from dotenv import load_dotenv
from guilded.ext import commands

# Open yaml configs
with open('config.yaml', 'r') as config_file:
    config = yaml.load(config_file, Loader=yaml.BaseLoader)

# Define 'client'
client = commands.Bot(command_prefix="//")

# Load token from .env file
load_dotenv()

# Connect to cogs
for f in os.listdir("./cogs"):
    if f.endswith(".py"):
        client.load_extension("cogs." + f[:-3])

# What do do on startup
@client.event
async def on_ready():
    print('Ready')

@client.event
async def on_ready():
    print(f'Logged in as {client.user}!')

# Get token from .env file
TOKEN = os.environ.get("BOT_TOKEN")

# Run the bot with the token
client.run(TOKEN)

